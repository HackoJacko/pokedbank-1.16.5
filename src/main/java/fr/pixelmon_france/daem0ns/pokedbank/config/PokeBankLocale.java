package fr.pixelmon_france.daem0ns.pokedbank.config;

import com.pixelmonmod.pixelmon.api.config.api.data.ConfigPath;
import com.pixelmonmod.pixelmon.api.config.api.yaml.AbstractYamlConfig;
import info.pixelmon.repack.org.spongepowered.objectmapping.ConfigSerializable;
import info.pixelmon.repack.org.spongepowered.objectmapping.meta.Setting;

@ConfigSerializable
@ConfigPath("config/pokedbank/locale.cfg")
public class PokeBankLocale extends AbstractYamlConfig {

    public PokeBankLocale() {

    }

    @Setting
    public String usage = "/pokebank <reload|applySpec> [username|specs,separated,with,comas] [specs,separated,with,comas]";

    @Setting
    public String onlyPlayers = "&cThis command can only be executed by a player";

    @Setting
    public String noPlayer = "&cThis player doesn't exist!";

    @Setting
    public String multipleServersAccess = "&cYou can't open the pokebank on two different servers at the same time!";

    @Setting
    public String noPermission = "&cYou don't have the permission to use this command!";

    @Setting
    public String reloaded = "Pokedbank config reloaded!";

    @Setting
    public String reloadError = "&cError while reloading the Pokebank config! See console for more information!";

    @Setting
    public String wrongUsage = "&cWrong usage!";

    @Setting
    public String editedBank = "Pokebank edited for all players!";

    @Setting
    public String editedSpecificBank = "Pokebank edited for %player%!";

    @Setting
    public String appliedSpecs = "Specs added: %specs%";

    @Setting
    public String emptyBankGUI = "Your Pokebank is empty!";

    @Setting
    public String emptyPartyGUI = "Your team is empty!";

    @Setting
    public String PokeBankGUITitle = "PokeBank - Save your Pokemon";

    @Setting
    public String notInDatabase = "This pokemon doesn't exist in the database...";

    @Setting
    public String unableToConvert = "Unable to convert pokemon";

    @Setting
    public String storageDisabled = "Adding pokemon to storage is currently disabled!";

    @Setting
    public String emptySlot = "This slot is empty...";

    @Setting
    public String maxStored = "You can't store any more pokemon, you can only store %amount%!";

    @Setting
    public String blacklistedHeldItem = "You can't store Pokemon with this blacklisted held item!";

    @Setting
    public String blacklistedPokemon = "You can't store this blacklisted pokemon!";

    @Setting
    public String blacklistedSpec = "You can't store a pokemon with the %spec% spec!";

    @Setting
    public String withHeldItem = "You can't store Pokemon with a held item!";

    @Setting
    public String storedFusedPokemon = "You can't store any fused Pokemon!";

    @Setting
    public String maxShiniesStored = "You can't store any more shiny Pokemon, you can only store %amount%";

    @Setting
    public String maxUltraBeastsStored = "You can't store any more Ultra Beasts, you can only store %amount%!";

    @Setting
    public String maxLegendariesStored = "You can't store any more Legendaries, you can only store %amount%!";

    @Setting
    public String maxTexturesStored = "You can't store any more textured Pokemon, you can only store %amount%!";

    @Setting
    public String maxSpecialsStored = "You can't store any more special Pokemon, you can only store %amount%!";

    @Setting
    public String fullParty = "Your party is full, you can't withdraw any Pokemon!";

    @Setting
    public String disabledWithdrawals = "Removing pokemon from storage is currently disabled!";

    @Setting
    public String storageError = "Failed to add to storage, please notify someone with console access!";

    @Setting
    public String noPokemon = "You need more pokemon to do that!";
}
